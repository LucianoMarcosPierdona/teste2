const Email = require("email-templates");

const email = new Email({
  message: {
    from: "lucianompj1@gmail.com",
  },
  transport: {
    jsonTransport: true,
  },
});

email
  .send({
    template: "jumpstart",
    message: {
      to: "luciano@jumpstartfilings.com.com",
    },
    locals: {
      firstName: "Luciano",
      payPeriod: "12-20-20",
      payrollSchedule: "Bi-Weekly",
      viewBankingInformationLink: "https://google.com",
      unsubscribeLink: "https://google.com",
    },
  })
  .then(console.log)
  .catch(console.error);
